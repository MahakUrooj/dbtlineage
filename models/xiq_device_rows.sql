{{ config(materialized='table')}}

with supported_combine_names as (
    select 
        role from {{source('mehak_xiq_source','xiq_xiq_device')}}
)

select *
from supported_combine_names
