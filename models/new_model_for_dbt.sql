{{ config(materialized='table')}}

with cdpaas_sheet as (
    select 
        concat(id,object) AS new_id from {{source('my_source','coupons')}}
)

select *
from cdpaas_sheet
