{{ config(materialized='table')}}

with supported_combine_names as (
    select 
        device_sn, connection_time from {{source('demo_source','xiq_ap_connection_time')}}
)

select *
from supported_combine_names
