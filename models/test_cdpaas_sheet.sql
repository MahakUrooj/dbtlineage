{{ config(materialized='table')}}

with cdpaas_sheet as (
    select 
        concat(path,comments) AS path_comments from {{source('dbt_source','_testsheetforccdpaas')}}
)

select *
from cdpaas_sheet